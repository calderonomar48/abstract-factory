package concretesProducts;

import abstractsProducts.Automovil;

public class AutomovilElectrico extends Automovil {

    StringBuffer salida =  new StringBuffer();

    public AutomovilElectrico(String model, String color, int potencia, double espacio) {
        super(model, color, potencia, espacio);
        mostrarCarateristicas();
    }

    @Override
    public void mostrarCarateristicas() {

        salida.append("Automovil  electrico  modelo");
        salida.append(model);
        salida.append("de color");
        salida.append(color);
        salida.append("de potencia");
        salida.append(potencia);
        salida.append("de espacio");
        salida.append(espacio);
    }
}
