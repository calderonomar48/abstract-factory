package builderPattern;

public interface IBuilder {
    BankAccount build();
}